let countLetter = (letter, sentence) => {
    let result = 0;

    if(letter.length === 1  && letter.match(/^[a-zA-Z]+$/) ){
        result=sentence.split(letter).length-1;
    }else{
         return undefined;
    }
    return result;
    
}


let isIsogram = (text) => {
    return !text.match(/([a-z]).*\1/i)
}

let purchase = (age, price) => {
    let result;
    if((age>=13 && age<=21) || age>64){
        result=price-(price *.20);
    }else if(age>=22 && age<=64){
        result=price;
    }else{
        return undefined
    }
    return String(result.toFixed(2));
}

let findHotCategories = (items) => {
    const result = items.filter((thing, index, self) =>
      index === self.findIndex((t) => (
        t.stocks === 0 && t.category === thing.category
      ))
    )

    let newR=[];
     for(let i=0 ; i<result.length ; ++i) {
         newR.push(result[i].category);
     }
    return newR;

}

let findFlyingVoters = (candidateA, candidateB) => {
    let common = [];                   // Array to contain common elements
      for(let i=0 ; i<candidateA.length ; ++i) {
        for(let j=0 ; j<candidateB.length ; ++j) {
          if(candidateA[i] == candidateB[j]) {      
            common.push(candidateA[i]);        
          }
        }
      }
  return common; 
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};